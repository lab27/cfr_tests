/**
 * Created with IntelliJ IDEA.
 * User: lee
 * Date: 11/01/2018
 * Time: 06:15
 */
class frob2() {

    fun fred2(x: Int = 300, y: frob2 = mkFrob2(x)) {
        println("${this}${x}${y}")
    }

    fun mkFrob2(x: Int): frob2 {
        return this;
    }

    fun foobar() {
        fred2();
        fred2(100);
        fred2(100, frob2());
    }
}