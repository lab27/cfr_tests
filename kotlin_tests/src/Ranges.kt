/**
 * Created with IntelliJ IDEA.
 * User: lee
 * Date: 10/01/2018
 * Time: 20:27
 */
fun foo() {
    for (i in 0..3) {            // 1
        print(i)
    }
    print(" ")

    for (i in 2..8 step 2) {     // 2
        print(i)
    }
    print(" ")

    for (c in 'a'..'d') {       // 3
        print(c)
    }
    print(" ")

    for (c in 'a'..'g' step 2) { // 4
        print(c)
    }
    print(" ")

    for (i in 3 downTo 0) {      // 5
        print(i)
    }
    print(" ")

    val x = 2
    if (x in 1..10) {            // 6
        print(x)
    }
    print(" ")

    val y = 3
    if (y !in 1..4) {            // 7
        print(y)
    }
}