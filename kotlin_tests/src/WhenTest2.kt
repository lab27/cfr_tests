/**
 * Created with IntelliJ IDEA.
 * User: lee
 * Date: 01/05/2018
 * Time: 06:22
 */
fun testBehave2(str  : String, xs : Array<Int>) = when (str) {
        "BB" -> 1;
        "Aa" -> xs.fold(0) { a,b -> a+b };
        "Ab" -> 3;
        else -> xs.fold(0, { a,b -> a*b });
    }
