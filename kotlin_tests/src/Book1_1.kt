/**
 * Created with IntelliJ IDEA.
 * User: lee
 * Date: 01/05/2018
 * Time: 05:51
 */
data class Person(val name : String, val age : Int? = null) {
    val isOld : Boolean
        get() {
            return age ?: 0 > 18;
        }
}

fun main(args : Array<String>) {
    val persons = listOf(Person("alice"), Person("bob", 29));
    val oldest = persons.maxBy { it.age ?: 0 };
    println("Oldest is $oldest");
}

fun max(a : Int, b:Int) = if (a > b) {a} else {b}

enum class Color(val red : Int, val green : Int, val blue:Int) {
    RED(255,0,0),
    WHITE(255,255,255),
    BLUE(0,0,255);

    val rgb : Int
        get() = red*65536 + green * 256 + blue;
}

fun behave(col : Color) = when (col) {
        Color.RED, Color.BLUE -> "Richard"
        Color.WHITE -> "Wha";
    }

fun behave(str  : String) = when (str) {
    "BB" -> "AB";
    "Aa" -> "cd";
    "Ab" -> "de";
    else -> "x";
}