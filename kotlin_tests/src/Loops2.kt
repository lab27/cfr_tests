/**
 * Created with IntelliJ IDEA.
 * User: lee
 * Date: 10/01/2018
 * Time: 20:25
 */
fun main(args: Array<String>) {
    var cakesEaten = 0
    var cakesBaked = 0

    while (cakesEaten < 10) {                   // 1
        eatACake()
        cakesEaten ++
    }

    do {                                        // 2
        bakeACake()
        cakesBaked++
    } while (cakesBaked < cakesEaten)

}

fun bakeACake() {}
fun eatACake() {}