/**
 * Created with IntelliJ IDEA.
 * User: lee
 * Date: 11/01/2018
 * Time: 06:15
 */
class frob() {
}

fun fred( x : Int = 300, y : frob = mkFrob(x)) {
    println("${x}${y}")
}

fun mkFrob(x : Int) : frob {
    return frob();
}

fun foobar() {
    fred();
    fred(100);
    fred(100,frob());
}