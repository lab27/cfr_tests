/**
 * Created with IntelliJ IDEA.
 * User: lee
 * Date: 01/05/2018
 * Time: 06:22
 */

fun scopeTest() {
    val result = mutableListOf<() -> Unit>()
    var i = 0
    for (j in 0..5) {
        val ii = i++
        result += { println("$i $ii $j") }
    }

    result.forEach { it() }
}