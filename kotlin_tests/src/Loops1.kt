/**
 * Created with IntelliJ IDEA.
 * User: lee
 * Date: 10/01/2018
 * Time: 20:23
 */
fun main(args: Array<String>) {
    val cakes = listOf("carrot", "cheese", "chocolate")

    for (cake in cakes) {                               // 1
        println("Yummy, it's a $cake cake!")
    }

}