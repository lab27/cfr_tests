/**
 * Created with IntelliJ IDEA.
 * User: lee
 * Date: 01/05/2018
 * Time: 07:21
 */
fun is1(i : Int) = if (i == 1) {true } else {false}

fun is1_b(i : Int) = (i == 1)

fun is1_c(i : Int) = when (i) {
    1 -> true
    else -> false
}

fun is1_d(i : Int) : Boolean { return when (i) {
    1 -> true
    else -> false
}}