/**
 * Created with IntelliJ IDEA.
 * User: lee
 * Date: 07/02/2018
 * Time: 18:28
 */

private tailrec fun Count(acc : Int, x : Int) : Int{
    return when {
        x == 0 -> acc
        else -> Count(acc + 1, x-1);
    }
}

private fun Count2(acc : Int, x : Int) : Int{
    return when {
        x == 0 -> acc
        else -> Count2(acc + 1, x-1);
    }
}