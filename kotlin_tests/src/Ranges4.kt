/**
 * Created with IntelliJ IDEA.
 * User: lee
 * Date: 12/01/2018
 * Time: 06:55
 */
fun foo4() {
    val y = 3
    if (y !in 1..4) {            // 7
        print(y)
    }
}