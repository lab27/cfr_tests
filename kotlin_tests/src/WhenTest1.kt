/**
 * Created with IntelliJ IDEA.
 * User: lee
 * Date: 01/05/2018
 * Time: 06:22
 */

/*fun testBehave(str  : String) = when (str) {
    "BB" -> 111;
    "Aa" -> 222;
    "Ab","cc" -> 333;
    else -> 444;
}
  */

fun testBehave(str  : String) = when (str) {
    "BB" -> 111;
    "Ab" -> 333;
    else -> 444;
}
