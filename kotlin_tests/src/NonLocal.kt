/**
 * Created with IntelliJ IDEA.
 * User: lee
 * Date: 01/05/2018
 * Time: 07:41
 */
fun anyPos(ints : Array<Int>) : Boolean {
    ints.forEach { if (it > 0) return true; }
    return false;
}

fun anyPos2(ints : Array<Int>, isPos : (Int) -> Boolean) : Boolean {
    val x = ints.map {
        return isPos(it);
    }
    println(x);
    return false;
}

fun anyPos3(ints : Array<Int>, isPos : (Int) -> Boolean) : Boolean {
    val x = ints.map { isPos(it)}
    println(x);
    return false;
}

fun main(args : Array<String>) {
    fun isPos (x : Int) : Boolean = x > 0;
    anyPos2(arrayOf(-1,-2,-1,2,0,1,2), ::isPos);
    anyPos3(arrayOf(-1,-2,-1,2,0,1,2), ::isPos);
}