/**
 * Created with IntelliJ IDEA.
 * User: lee
 * Date: 01/05/2018
 * Time: 06:22
 */

fun whenSwitch(str  : String) = when (str) {
    "Aa", "BB" -> 111;
    "cc" -> 222;
    else -> 444;
}