/**
 * Created with IntelliJ IDEA.
 * User: lee
 * Date: 10/01/2018
 * Time: 20:15
 */
fun main() {
    val greeting = "Kotliner"

    println("Hello $greeting")                  // 1
    println("Hello ${greeting.toUpperCase()}")  // 2
}