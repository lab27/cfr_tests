package org.benf.cfr.tests;

class InnerClassTest32 {
    private static class A {
        private void f(final String s) {
        }

        void g(final String s) {
        }

        class B extends A {
            private void f(final String s) {
            }

            void g(final String s) {
            }

            class C extends B {
                private void f(final String s) {
                }

                void g(final String s) {
                }

                void test1() {
                    A.this.f("A.f()");
                    B.this.f("B.f()");
                    B.super.f("A.f()");
                    C.super.f("B.f()");
                    C.this.f("C.f()");
                    super.f("B.f()");
                    this.f("C.f()");
                }

                void test2() {
                    A.this.g("A.g()");
                    B.this.g("B.g()");
                    B.super.g("A.g()");
                    C.super.g("B.g()");
                    C.this.g("C.g()");
                    super.g("B.g()");
                    this.g("C.g()");
                }
            }
        }
    }
}
