package org.benf.cfr.tests;

import java.util.function.Supplier;

/**
 * Created with IntelliJ IDEA.
 * User: lee
 * Date: 18/08/2013
 * Time: 21:37
 */
public class VoidTest {

    public static Class<?> a() {
        return void.class;
    }

    public static Class<?> b() {
        return Void.class;
    }
}
