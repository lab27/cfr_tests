#!/usr/bin/perl
use File::Slurp;

# use -wB to ignore blanks

my $numArgs = $#ARGV + 1;
my $arg = "$ARGV[$numArgs - 1]";
my $base = "";
my @files = `ls -1 expected-procyon`;

# if ($arg ne "") { @files = grep($arg, @files); }

if ($arg ne "") { @files = `ls -1 expected-procyon | grep $arg`; }

chomp @files;

foreach my $file (@files) {
    print "[$file]";
    my $path = "../out/production/cfr_tests/org/benf/cfr/tests/$file.class";
    my $cmd = "java -ea -DAnsi=false com.strobel.decompiler.DecompilerDriver $path";
    my $actualtext = `$cmd`;
    my $expected = read_file("expected-procyon/$file");
    if ($actualtext ne $expected) {
        `$cmd > /tmp/cfrtest.tmp`;
        $res = system("diff expected-procyon/$file /tmp/cfrtest.tmp");	      
        print "($res)\n";
        print "[$cmd > expected-procyon/$file]\n";
        print " ** FAIL ** Accept new? (y?)";
        my $key = <STDIN>;
	chomp $key;        
        if ($key eq "y") {
	  `cp /tmp/cfrtest.tmp expected-procyon/$file`;
        }
    }
    print "\n";
}	
