#!/usr/bin/perl
use File::Slurp;

# use -wB to ignore blanks

my $pattern = $ARGV[0];
my $base = "";
my @files = `ls -1 expected`;
chomp @files;

if ($arg ne "") { @files = grep($arg, @files); }

foreach my $file (@files) {
    if ($file =~ /$pattern/) {
    print "[$file]";
    my $path = "../output/JAVA_8_B132/org/benf/cfr/tests/$file.class";
    my $cmd = "java -jar  ~/website/other/cfr/cfr_0_87.jar $path --showversion false";
    my $actualtext = `$cmd`;
    my $expected = read_file("expected/$file");
    if ($actualtext ne $expected) {
        `$cmd > /tmp/cfrtest.tmp`;
        $res = system("diff expected/$file /tmp/cfrtest.tmp");	      
        print "($res)\n";
        print "[$cmd > expected/$file]\n";
        print " ** FAIL ** Accept new? (y?)";
        my $key = "y"; # <STDIN>;
	chomp $key;        
        if ($key eq "y") {
	  `cp /tmp/cfrtest.tmp expected/$file`;
       }
    }
    print "\n";
   }
}	
