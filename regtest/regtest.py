#!/usr/bin/python

import os
import sys
import shutil
import subprocess
import itertools

def update_if_accepted(message, patha, pathb):
  print (message)
  response = ""
  print ("Was : " + pathb + "\nNow : " + patha + "\n")
  print ("Accept new? (y/n/b[ad])\n")
  while response != "y" and response != "n" and response != "b":
    response = sys.stdin.read(1).lower()
  if response == "y":
    shutil.copy(patha, pathb)
  elif response == "b":
    with open(pathb, "w") as text_file:
      text_file.write("Bad decompilation - fix!")

shutil.rmtree("temptestfiles", ignore_errors=True)
os.mkdir("temptestfiles")

filter = "*" if len(sys.argv) < 2 else "*" + sys.argv[1] + "*"
clspath = "../output/JAVA_8_B132/org/benf/cfr/tests/" + filter + ".class"

subprocess.call("java -classpath /Users/lee/code/cfr/out/production/cfr org.benf.cfr.reader.Main " + clspath + " --showversion false --renameillegalidents true --outputdir temptestfiles", shell=True)

for path in itertools.chain(*[[a+"/"+x for x in c] for (a,b,c) in list(os.walk('temptestfiles'))]):
  if not(path.endswith(".java")):
    continue
  (_, file) = os.path.split(path)
  file = file[:-5]
  expected_path = "expected/" + file
  print ("Checking " + file)

  if not(os.path.isfile(expected_path)):
    with open(path, 'r') as newfile:
      data = newfile.read()
    update_if_accepted(data + "\n--------New result for " + file, path, expected_path)
  else:    
    (res, diff) = subprocess.getstatusoutput("diff " + expected_path + " " + path)
    if res != 0:
      update_if_accepted(diff + "\n---------Difference in " + file, path, expected_path)
