/*
 * Decompiled with CFR.
 */
package org.benf.cfr.tests;

public class CondJumpTestInlineAssign1 {
    boolean thisa;

    public boolean test(boolean a, boolean b) {
        this.thisa = a;
        return b == this.thisa;
    }
}
