/*
 * Decompiled with CFR.
 */
package org.benf.cfr.tests;

import java.io.PrintStream;

public class ShortCircuitAssignTest6 {
    static boolean[] c;

    public boolean test1(boolean a, boolean b) {
        System.out.println(b && a == (ShortCircuitAssignTest6.c[0] = b) && b || !c[0]);
        return c[0];
    }

    public boolean test2(boolean a, boolean b) {
        System.out.println(b && a == (ShortCircuitAssignTest6.c[0] = b) || !c[0]);
        return c[0];
    }

    public boolean test3(boolean a, boolean b) {
        System.out.println(b && a || (ShortCircuitAssignTest6.c[0] = b) || !c[0]);
        return c[0];
    }

    public boolean test4(boolean a, boolean b) {
        System.out.println(b && (ShortCircuitAssignTest6.c[0] = a) || !c[0]);
        return c[0];
    }

    public boolean test5(boolean a, boolean b) {
        System.out.println(b || (ShortCircuitAssignTest6.c[0] = a) || !c[0]);
        return c[0];
    }

    public boolean test6(boolean a, boolean b) {
        System.out.println(b && (ShortCircuitAssignTest6.c[0] = a));
        return c[0];
    }

    public boolean test7(boolean a, boolean b) {
        System.out.println(b || (ShortCircuitAssignTest6.c[0] = a));
        return c[0];
    }

    public boolean test8(boolean a, boolean b) {
        System.out.println(b && a == (ShortCircuitAssignTest6.c[0] = b) && b && c[0]);
        return c[0];
    }
}
