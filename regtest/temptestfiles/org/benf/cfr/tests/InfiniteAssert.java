/*
 * Decompiled with CFR.
 */
package org.benf.cfr.tests;

import java.io.PrintStream;

public class InfiniteAssert {
    public static void f1(String[] args) {
        while (true) {
            assert (false);
        }
    }

    public static void f2(String[] args) {
        while (true) {
            assert (false);
        }
    }

    public static void f1a(String[] args) {
        do {
            // Infinite loop
        } while (true);
    }

    public static void f2a(String[] args) {
        do {
            // Infinite loop
        } while (true);
    }

    public static void f3(String[] args) {
        while (true) {
            assert (args.length < 3);
        }
    }

    public static void f4(String[] args) {
        do {
            assert (args.length < 3);
            System.out.print(2);
        } while (true);
    }

    public static void f5(String[] args) {
        do {
            System.out.print(2);
            assert (args.length < 3);
        } while (true);
    }
}
