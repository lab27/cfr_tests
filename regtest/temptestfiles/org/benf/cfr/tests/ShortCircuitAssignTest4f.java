/*
 * Decompiled with CFR.
 */
package org.benf.cfr.tests;

import java.io.PrintStream;

public class ShortCircuitAssignTest4f {
    public boolean test1(boolean a, boolean b, boolean c, boolean d) {
        c = a ? c : d;
        System.out.println(c && ((c = a) ? c : d));
        return a;
    }
}
