/*
 * Decompiled with CFR.
 */
package org.benf.cfr.tests;

import java.io.PrintStream;

public class ShortCircuitAssignTest4b {
    public boolean test1(boolean a, boolean b, boolean[] c, boolean[] d) {
        System.out.println(b && null != (c = a ? c : d) && a == (c[0] = b) && b || !c[0]);
        return c[0];
    }
}
