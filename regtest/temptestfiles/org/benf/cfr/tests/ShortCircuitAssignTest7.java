/*
 * Decompiled with CFR.
 */
package org.benf.cfr.tests;

import java.io.PrintStream;

public class ShortCircuitAssignTest7 {
    static int i;
    int j;

    private static int index() {
        return 0;
    }

    public boolean test1(boolean a, boolean b, boolean[] c) {
        System.out.println(a && c[0] == (c[0] = b) && b || !c[0]);
        return c[0];
    }

    public boolean test2(boolean a, boolean b, boolean[] c) {
        System.out.println(b && a == (c[0] = !c[0]) || !c[0]);
        return c[0];
    }

    public boolean test3(boolean a, boolean b, boolean[] c) {
        System.out.println(a && c[ShortCircuitAssignTest7.index()] == (c[ShortCircuitAssignTest7.index()] = b) && b || !c[ShortCircuitAssignTest7.index()]);
        return c[ShortCircuitAssignTest7.index()];
    }

    public boolean test4(boolean a, boolean b, boolean[] c) {
        System.out.println(b && a == (c[ShortCircuitAssignTest7.index()] = !c[ShortCircuitAssignTest7.index()]) || !c[ShortCircuitAssignTest7.index()]);
        return c[ShortCircuitAssignTest7.index()];
    }

    public boolean test5(boolean a, boolean b, boolean[] c) {
        System.out.println(a && c[ShortCircuitAssignTest7.index() + i] == (c[ShortCircuitAssignTest7.index() + this.j] = b) && b || !c[ShortCircuitAssignTest7.index() + this.j]);
        return c[ShortCircuitAssignTest7.index() + i];
    }

    public boolean test6(boolean a, boolean b, boolean[] c) {
        System.out.println(b && a == (c[ShortCircuitAssignTest7.index() + ShortCircuitAssignTest7.i] = !c[ShortCircuitAssignTest7.index() + this.j]) || !c[ShortCircuitAssignTest7.index() + this.j]);
        return c[ShortCircuitAssignTest7.index() + i];
    }
}
