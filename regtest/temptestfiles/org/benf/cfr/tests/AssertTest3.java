/*
 * Decompiled with CFR.
 */
package org.benf.cfr.tests;

import java.io.PrintStream;
import java.util.Map;
import org.benf.cfr.tests.support.MapFactory;

public class AssertTest3 {

    private static class Inner {
        static final Map<String, String> map = MapFactory.newMap();
        static final Map<String, String> map2 = MapFactory.newMap();

        private Inner() {
        }

        public void test1(String s) {
            assert (!s.equals("Fred"));
            System.out.println(s);
        }
    }

}
