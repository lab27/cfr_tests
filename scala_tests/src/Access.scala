/**
  * Created with IntelliJ IDEA.
  * User: lee
  * Date: 12/06/2017
  * Time: 19:09
  */
class Access {
  def foo() : Unit = {
    def z = Access.x;
  }
}

object Access {
  private def x : Int =  3;
}
