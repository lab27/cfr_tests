/**
 * Created with IntelliJ IDEA.
 * User: lee
 * Date: 07/06/17
 * Time: 18:32
 */
class LambdaTest {
  def test = {
    val s = "foo"
    (x: String) => x == s
  }
}
