/**
 * Created with IntelliJ IDEA.
 * User: lee
 * Date: 07/06/17
 * Time: 18:32
 */

abstract class Ab {
  def foo():Int
}

class LambdaTest2 {
  def test() : Ab = {
            new Ab {
              val lmb = Array(1,2).map { (a:Int) => null };
              override def foo() : Int = 3
            }
  }
}
