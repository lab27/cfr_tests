/**
  * Created with IntelliJ IDEA.
  * User: lee
  * Date: 12/06/2017
  * Time: 18:58
  */

abstract class Ab2 {
  def foo():Int
}

class LambdaTest3 {
  def test() : Ab2 = {
    val lmb = Array(1,2).map { (a:Int) => null };
    new Ab2 {
      override def foo() : Int = 3
    }
  }
}
