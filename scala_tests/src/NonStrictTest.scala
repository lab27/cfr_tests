/**
  * Created with IntelliJ IDEA.
  * User: lee
  * Date: 27/06/2017
  * Time: 06:11
  */
class NonStrictTest {
  def bar(a: => Int) : Int = {
    println ("Bar");
    a + a
  }

  def foo(a: => Int) : Int = {
    println("Foo");
    lazy val b = a;
    bar(b) + bar(b)
  }
}

object NonStrictTest {
  def g(x:Int): Int = {
    print("Evaluating g\n")
    x
  }

  def main(args : Array[String]): Unit = {
    def x = new NonStrictTest()
    x.foo(g(3))
  }
}
