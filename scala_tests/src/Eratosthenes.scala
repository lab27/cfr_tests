/**
  * Created with IntelliJ IDEA.
  * User: lee
  * Date: 22/06/2017
  * Time: 18:54
  */
class Eratosthenes {
  def Sieve(count : Int) : Array[Int] = {
    def list = Stream.from(2);

    def rest(str : Stream[Int]) : Stream[Int] = {
      def hv = str.head
      hv #:: rest(str.tail.filter(_ % hv != 0))
    }

    rest(list).take(count).toArray
  }
}

object Eratosthenes {
  def main(args : Array[String]) =
     print (new Eratosthenes().Sieve(args(0).toInt).mkString(", "))
}